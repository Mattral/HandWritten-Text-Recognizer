# Hand-Digit-Recognizer

EDA, and data pipeline and deployed on streamlit

The [link](https://www.kaggle.com/datasets/nibinv23/iam-handwriting-word-database) of kaggle dataset.

Streamlit Web Interface for Handwritten Text Recognition (HTR), Optical Character Recognition (OCR) implemented with TensorFlow and trained on the IAM off-line HTR dataset. The model takes images of single words or text lines (multiple words) as input and outputs the recognized text.
